import pandas as pd
import numpy as np
import re
import argparse


def new_features(columns):
    '''
    Prepend custom feature names with "f0" 
    :param columns: List of features columsn
    '''
    output_col = []
    for col in columns:
        if bool(re.match('f[0-9]{1,2}\_', col)):
            output_col.append(col)
        else:
            output_col.append('f0_' + col)
    return output_col


def set_level_cap(data_orginal, data_features, threshold=10):
    '''
    Lump categorical levels for features with high number of categorical levels.
    Marks a_level_cap as "yes" and set v_level_cap.

    :param  data_original: df with sample data
    :param  data_features: df feature config for categorical featuers
    :param  threshold: Features with count of categorical levels greater than the threshold will be capped
                       to the threshold. default=10
    '''
    nunique_data = data_orginal.select_dtypes('object').nunique()
    nunique_cut = nunique_data[nunique_data > threshold].index.to_list()

    df_output = data_features.copy()
    df_output.loc[df_output['feature'].isin(nunique_cut), ['a_level_cap', 'p_level_cap']] = [
        'yes', f'[\'top\', \'{threshold}\']']

    return df_output


def numerical_missing_impute(data_features, selection_criteria, impute_value):
    '''
    Changes impute logic for different featuers
    '''
    df_output = data_features.copy()
    df_output.loc[df_output['feature'].str.lower()
                  .str.contains('|'.join(selection_criteria)), ['a_miss_impute',
                                                                'p_miss_impute']] = ['yes', impute_value]
    return df_output


def manual_ignore(data):
    '''
    Hard coded list of features to mark as ignore and nan
    '''
    ignore_list = ['f01_area_code', 'f01_area_code_new', 'f01_mem_mail_add_postcode_flag',
                   'f01_mem_mail_address_postcode', 'f01_mem_preferred_store', 'f01_mem_res_add_postcode_flag',
                   'f01_mem_resi_address_postcode', 'f01_store_name', 'f01_mem_cvm_end_date']
    output_data = data.copy()

    output_data.loc[output_data['feature'].isin(
        ignore_list), 'input'] = 'ignore'
    output_data.loc[output_data['feature'].isin(ignore_list),
                    ['a_miss_impute', 'a_level_cap', 'a_one_hot',
                     'a_label', 'a_binary', 'a_uint8',
                     'p_miss_impute', 'p_level_cap']] = 'nan'
    return output_data


def extract_table_name(x):
    '''
    Function to be used with pandas apply
    :param x: "new_feature" values as string
    '''
    table_name = x.split('_')[0]
    return table_name


def generate_excel(df, output_path, sample_excel_path, cat_threshold):
    df = df.drop(['crn', 'ref_dt'], axis=1)

    # get columns by type
    df_types = df.dtypes
    categorical_columns = df_types[df_types == 'object'].index.to_list()
    numeric_columns = df_types[df_types != 'object'].index.to_list()

    #######################
    # CATEGORICAL FEATURES
    #######################
    print('Categorical feature processing')

    cat_df_header = pd.read_excel(
        sample_excel_path, sheet_name="feature_spec_cat").columns
    df_categorical = pd.DataFrame(columns=cat_df_header)

    df_categorical['feature'] = categorical_columns
    df_categorical['new_feature'] = new_features(categorical_columns)
    df_categorical['table'] = df_categorical['new_feature'].apply(
        extract_table_name)

    df_categorical['input'] = 'input'
    df_categorical['a_label'] = 'yes'
    df_categorical['a_miss_impute'] = 'no'
    df_categorical[['v_miss_impute', 'v_level_cap',
                    'v_one_hot', 'v_label']] = ''

    df_categorical = set_level_cap(df, df_categorical, threshold=cat_threshold)
    df_categorical = manual_ignore(df_categorical)
    df_categorical = df_categorical.fillna('nan')
    print('Done')

    #######################
    # NUMERICAL FEATURES
    #######################
    print('Numercial feature processing')

    num_df_header = pd.read_excel(
        sample_excel_path, sheet_name="feature_spec_num").columns
    df_numerical = pd.DataFrame(columns=num_df_header)
    df_numerical['feature'] = numeric_columns
    df_numerical['new_feature'] = new_features(numeric_columns)
    df_numerical['table'] = df_numerical['new_feature'].apply(
        extract_table_name)

    df_numerical['input'] = 'input'
    df_numerical[['a_outlier_cap', 'a_normalise',
                  'a_miss_impute', 'a_miss_flag',
                  'a_binary', 'a_uint8',
                  'a_bucketing']] = 'no'

    df_numerical[['v_outlier_cap', 'v_normalise',
                  'v_miss_impute', 'v_bucketing']] = ''

    columns_zero_impute = ['spent', 'spend', 'dollar', 'amt', 'amount',
                           'cnt', '_mem_duration_cell', 'segment_change_', '_lifestage_change_', 'dsct',
                           'flx_trn_', '_txn_', 'flx_avg_baskets', '_shopped_',
                           'flx_visit_', 'flx_sales', '_visit_', 'spd',
                           '_wkshopped', '_avgspd_', '_churn_flag_', 'avg_tenure_', 'mem_tenure',
                           '_earn_rate_', 'card_number_total', '_click_', '_open_', '_activat',
                           '_sent_', '_scan_rate_', '_tag_rate_', '_sales_pct_', '_no_rdmable_',
                           '_spofln_', '_avg_basket_', '_lylty_sales_', '_lylty_basket_']

    columns_median_impute = [
        'dist_',  '_comp_wi_',  '_5km', '_500m', '_mem_age']

    columns_max_impute = ['_int_', 'weeks_since_']

    df_numerical = numerical_missing_impute(
        df_numerical, columns_zero_impute, 'zero')
    df_numerical = numerical_missing_impute(
        df_numerical, columns_median_impute, 'median')
    df_numerical = numerical_missing_impute(
        df_numerical, columns_max_impute, 'max')

    df_numerical = df_numerical.fillna('nan')

    print('Done')
    ##############################
    # SET UP FOR EXCEL AND EXPORT
    ##############################
    print('Writing Excel config file')

    writer = pd.ExcelWriter(output_path, engine='xlsxwriter')

    df_tables = pd.read_excel(
        sample_excel_path, sheet_name="tables")
    df_mars_data_dictionary = pd.read_excel(
        sample_excel_path, sheet_name='mars_data_dictionary')
    df_constrain = pd.read_excel(
        sample_excel_path,  sheet_name='Constrain')

    df_tables.to_excel(writer, sheet_name='tables', index=False)
    df_mars_data_dictionary.to_excel(
        writer, sheet_name='mars_data_dictionary', index=False)
    df_numerical.to_excel(writer, sheet_name='feature_spec_num', index=False)
    df_categorical.to_excel(writer, sheet_name='feature_spec_cat', index=False)
    df_constrain.to_excel(writer, sheet_name='Constrain', index=False)

    writer.save()

    print('All completed successfully')


    
    
def generate_excel_config(df, cat_threshold):
    
    sample_excel_path = 'config_file_templates/excel_base.xlsx'
    df = df.drop(['crn', 'ref_dt'], axis=1)
    
    # get columns by type
    df_types = df.dtypes
    categorical_columns = df_types[df_types == 'object'].index.to_list()
    numeric_columns = df_types[df_types != 'object'].index.to_list()

    #######################
    # CATEGORICAL FEATURES
    #######################
    #print('Categorical feature processing')

    cat_df_header = pd.read_excel(
        sample_excel_path, sheet_name="feature_spec_cat").columns
    df_categorical = pd.DataFrame(columns=cat_df_header)

    df_categorical['feature'] = categorical_columns
    df_categorical['new_feature'] = new_features(categorical_columns)
    df_categorical['table'] = df_categorical['new_feature'].apply(
        extract_table_name)

    df_categorical['input'] = 'input'
    df_categorical['a_label'] = 'yes'
    df_categorical['a_miss_impute'] = 'no'
    df_categorical[['v_miss_impute', 'v_level_cap',
                    'v_one_hot', 'v_label']] = ''

    df_categorical = set_level_cap(df, df_categorical, threshold=cat_threshold)
    df_categorical = manual_ignore(df_categorical)
    df_categorical = df_categorical.fillna('nan')
    #print('Done')

    #######################
    # NUMERICAL FEATURES
    #######################
    #print('Numercial feature processing')

    num_df_header = pd.read_excel(
        sample_excel_path, sheet_name="feature_spec_num").columns
    df_numerical = pd.DataFrame(columns=num_df_header)
    df_numerical['feature'] = numeric_columns
    df_numerical['new_feature'] = new_features(numeric_columns)
    df_numerical['table'] = df_numerical['new_feature'].apply(
        extract_table_name)

    df_numerical['input'] = 'input'
    df_numerical[['a_outlier_cap', 'a_normalise',
                  'a_miss_impute', 'a_miss_flag',
                  'a_binary', 'a_uint8',
                  'a_bucketing']] = 'no'

    df_numerical[['v_outlier_cap', 'v_normalise',
                  'v_miss_impute', 'v_bucketing']] = ''

    columns_zero_impute = ['spent', 'spend', 'dollar', 'amt', 'amount',
                           'cnt', '_mem_duration_cell', 'segment_change_', '_lifestage_change_', 'dsct',
                           'flx_trn_', '_txn_', 'flx_avg_baskets', '_shopped_',
                           'flx_visit_', 'flx_sales', '_visit_', 'spd',
                           '_wkshopped', '_avgspd_', '_churn_flag_', 'avg_tenure_', 'mem_tenure',
                           '_earn_rate_', 'card_number_total', '_click_', '_open_', '_activat',
                           '_sent_', '_scan_rate_', '_tag_rate_', '_sales_pct_', '_no_rdmable_',
                           '_spofln_', '_avg_basket_', '_lylty_sales_', '_lylty_basket_']

    columns_median_impute = [
        'dist_',  '_comp_wi_',  '_5km', '_500m', '_mem_age']

    columns_max_impute = ['_int_', 'weeks_since_']

    df_numerical = numerical_missing_impute(
        df_numerical, columns_zero_impute, 'zero')
    df_numerical = numerical_missing_impute(
        df_numerical, columns_median_impute, 'median')
    df_numerical = numerical_missing_impute(
        df_numerical, columns_max_impute, 'max')

    df_numerical = df_numerical.fillna('nan')

    #print('Done')
    ##############################
    # SET UP FOR EXCEL AND EXPORT
    ##############################
    #print('Writing Excel config file')
    
    
    

    df_tables = pd.read_excel(
        sample_excel_path, sheet_name="tables")
    df_mars_data_dictionary = pd.read_excel(
        sample_excel_path, sheet_name='mars_data_dictionary')
    df_constrain = pd.read_excel(
        sample_excel_path,  sheet_name='Constrain')
    
    excel_dict = {
        'tables':df_tables,
        'mars_data_dictionary':df_mars_data_dictionary,
        'feature_spec_num':df_numerical,
        'feature_spec_cat':df_categorical,
        'Constrain':df_constrain
    }

    return excel_dict
    
    
def main():
    ############
    # ARG PARSE
    ############

    program_desc = 'Python cli tool to generate an excel config file from your cmd output.'

    parser = argparse.ArgumentParser(description=program_desc)
    parser.add_argument('-i', '--input', dest='inputfile', type=str,
                        required=True,
                        help='parquet file that contains columns/data from cmd')
    parser.add_argument('-o', '--output', dest='outputfile',  type=str,
                        default='feature-config.xlsx',
                        help='Output path for the excel config file generated by this tool. default=feature-config.xlsx')
    parser.add_argument('-t', '--threshold', dest='threshold', type=int, default=10,
                        help='Threshold for categorical level capping threshold. default=10')

    args = parser.parse_args()
    input_path = args.inputfile
    output_path = args.outputfile
    cat_threshold = args.threshold

    print(f'Input path: {input_path}')
    print(f'Output path: {output_path}')
    print(f'Categorical cap threshold: {cat_threshold}')

    #############
    # READ FILES
    # ###########

    print("Reading file")
    df = pd.read_parquet(input_path)
    print('Done')

    df = df.drop(['crn', 'ref_dt'], axis=1)

    # get columns by type
    df_types = df.dtypes
    categorical_columns = df_types[df_types == 'object'].index.to_list()
    numeric_columns = df_types[df_types != 'object'].index.to_list()

    #######################
    # CATEGORICAL FEATURES
    #######################
    print('Categorical feature processing')

    cat_df_header = pd.read_excel(
        'sample-data/sample_config.xlsx', sheet_name="feature_spec_cat").columns
    df_categorical = pd.DataFrame(columns=cat_df_header)

    df_categorical['feature'] = categorical_columns
    df_categorical['new_feature'] = new_features(categorical_columns)
    df_categorical['table'] = df_categorical['new_feature'].apply(
        extract_table_name)

    df_categorical['input'] = 'input'
    df_categorical['a_label'] = 'yes'
    df_categorical['a_miss_impute'] = 'no'
    df_categorical[['v_miss_impute', 'v_level_cap',
                    'v_one_hot', 'v_label']] = ''

    df_categorical = set_level_cap(df, df_categorical, threshold=cat_threshold)
    df_categorical = manual_ignore(df_categorical)
    df_categorical = df_categorical.fillna('nan')
    print('Done')

    #######################
    # NUMERICAL FEATURES
    #######################
    print('Numercial feature processing')

    num_df_header = pd.read_excel(
        'sample-data/sample_config.xlsx', sheet_name="feature_spec_num").columns
    df_numerical = pd.DataFrame(columns=num_df_header)
    df_numerical['feature'] = numeric_columns
    df_numerical['new_feature'] = new_features(numeric_columns)
    df_numerical['table'] = df_numerical['new_feature'].apply(
        extract_table_name)

    df_numerical['input'] = 'input'
    df_numerical[['a_outlier_cap', 'a_normalise',
                  'a_miss_impute', 'a_miss_flag',
                  'a_binary', 'a_uint8',
                  'a_bucketing']] = 'no'

    df_numerical[['v_outlier_cap', 'v_normalise',
                  'v_miss_impute', 'v_bucketing']] = ''

    columns_zero_impute = ['spent', 'spend', 'dollar', 'amt', 'amount',
                           'cnt', '_mem_duration_cell', 'segment_change_', '_lifestage_change_', 'dsct',
                           'flx_trn_', '_txn_', 'flx_avg_baskets', '_shopped_',
                           'flx_visit_', 'flx_sales', '_visit_', 'spd',
                           '_wkshopped', '_avgspd_', '_churn_flag_', 'avg_tenure_', 'mem_tenure',
                           '_earn_rate_', 'card_number_total', '_click_', '_open_', '_activat',
                           '_sent_', '_scan_rate_', '_tag_rate_', '_sales_pct_', '_no_rdmable_',
                           '_spofln_', '_avg_basket_', '_lylty_sales_', '_lylty_basket_']

    columns_median_impute = [
        'dist_',  '_comp_wi_',  '_5km', '_500m', '_mem_age']

    columns_max_impute = ['_int_', 'weeks_since_']

    df_numerical = numerical_missing_impute(
        df_numerical, columns_zero_impute, 'zero')
    df_numerical = numerical_missing_impute(
        df_numerical, columns_median_impute, 'median')
    df_numerical = numerical_missing_impute(
        df_numerical, columns_max_impute, 'max')

    df_numerical = df_numerical.fillna('nan')

    print('Done')
    ##############################
    # SET UP FOR EXCEL AND EXPORT
    ##############################
    print('Writing Excel config file')

    writer = pd.ExcelWriter(output_path, engine='xlsxwriter')

    df_tables = pd.read_excel(
        'sample-data/sample_config.xlsx', sheet_name="tables")
    df_mars_data_dictionary = pd.read_excel(
        'sample-data/sample_config.xlsx', sheet_name='mars_data_dictionary')
    df_constrain = pd.read_excel(
        'sample-data/sample_config.xlsx',  sheet_name='Constrain')

    df_tables.to_excel(writer, sheet_name='tables', index=False)
    df_mars_data_dictionary.to_excel(
        writer, sheet_name='mars_data_dictionary', index=False)
    df_numerical.to_excel(writer, sheet_name='feature_spec_num', index=False)
    df_categorical.to_excel(writer, sheet_name='feature_spec_cat', index=False)
    df_constrain.to_excel(writer, sheet_name='Constrain', index=False)

    writer.save()

    print('All completed successfully')


if __name__ == '__main__':
    main()
